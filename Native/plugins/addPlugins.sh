#! /bin/sh

cordova plugin add ../plugins/org.apache.cordova.splashscreen
cordova plugin add ../plugins/org.apache.cordova.network-information
cordova plugin add ../plugins/org.apache.cordova.file
cordova plugin add ../plugins/org.apache.cordova.file-transfer
cordova plugin add ../plugins/org.apache.cordova.media
cordova plugin add ../plugins/org.apache.cordova.vibration
cordova plugin add ../plugins/org.apache.cordova.media-capture
cordova plugin add ../plugins/org.apache.cordova.inappbrowser
cordova plugin add ../plugins/org.apache.cordova.globalization
cordova plugin add ../plugins/org.apache.cordova.geolocation
cordova plugin add ../plugins/org.apache.cordova.dialogs
cordova plugin add ../plugins/org.apache.cordova.device-orientation
cordova plugin add ../plugins/org.apache.cordova.device
cordova plugin add ../plugins/org.apache.cordova.contacts
cordova plugin add ../plugins/org.apache.cordova.console
cordova plugin add ../plugins/org.apache.cordova.camera
cordova plugin add ../plugins/org.apache.cordova.device-motion
cordova plugin add ../plugins/org.apache.cordova.battery-status
cordova plugin add ../plugins/org.apache.cordova.statusbar
cordova plugin add ../plugins/com.phonegap.plugins.barcodescanner
cordova plugin add ../plugins/com.justep.cordova.plugin.push
cordova plugin add ../plugins/com.justep.cordova.plugin.weixin