#! /bin/sh

cordova plugin remove org.apache.cordova.splashscreen
cordova plugin remove org.apache.cordova.network-information
cordova plugin remove org.apache.cordova.file-transfer
cordova plugin remove org.apache.cordova.media-capture
cordova plugin remove org.apache.cordova.media
cordova plugin remove org.apache.cordova.file
cordova plugin remove org.apache.cordova.vibration
cordova plugin remove org.apache.cordova.inappbrowser
cordova plugin remove org.apache.cordova.globalization
cordova plugin remove org.apache.cordova.geolocation
cordova plugin remove org.apache.cordova.dialogs
cordova plugin remove org.apache.cordova.device-orientation
cordova plugin remove org.apache.cordova.device
cordova plugin remove org.apache.cordova.contacts
cordova plugin remove org.apache.cordova.console
cordova plugin remove org.apache.cordova.camera
cordova plugin remove org.apache.cordova.device-motion
cordova plugin remove org.apache.cordova.battery-status
cordova plugin remove org.apache.cordova.statusbar
cordova plugin remove com.phonegap.plugins.barcodescanner
cordova plugin remove com.justep.cordova.plugin.push
cordova plugin remove com.justep.cordova.plugin.weixin
